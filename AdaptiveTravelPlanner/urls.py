"""AdaptiveTravelPlanner URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from Zomato.views import simplePageZomato
from PastTravels.views import pagesIndex, pastTravels
from survey.views import showSurvey, surveySubmitted
from django.contrib.auth.views import LoginView, LogoutView
from userProfile.views import ProfileDetailView, HomeView, RegisterView
from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('index/', pagesIndex, name='index'),
    path('admin/', admin.site.urls),
    path('register/', RegisterView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('', HomeView.as_view(), name='home'),
    path('survey/', showSurvey, name='survey'),
    path('survey_submit/', surveySubmitted),

    # Past Travels
    path('pastTravels/', include('PastTravels.urls')),
    path('recommendation/', include('Recommendation.urls')),

    path('view_model', include('UserModel.urls')),

    path('simplepageZomato/faf', simplePageZomato),
    path('profile/', login_required(ProfileDetailView.as_view()),
         name='profile')
]

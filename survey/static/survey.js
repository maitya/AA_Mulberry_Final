function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

Survey
    .StylesManager
    .applyTheme("default");

var city;

var cuisinesArray = ['Irish', 'Japanese', 'British', 'Italian', 'Thai', 'French', 'Spanish', 'Indonesian', 'Chinese', 'Mexican', 'Indian', 'American', 'Middle Eastern', 'Vietnamese', 'Moroccan', 'Australian', 'Brazilian', 'Polish', 'Pakistani', 'Turkish']
;
$.getJSON('http://ip-api.com/json', function (ip) {
    city = ip.city;
    console.log(ip.city);

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        headers: {
            'user-key': 'ec3d600095d546649779df426ec7232b',
        }
    });

    $.getJSON('https://developers.zomato.com/api/v2.1/cities?q=' + city, function (dataID) {
        var cityID = dataID.location_suggestions[0].id;
        console.log(dataID.location_suggestions[0].id);

        cuisinesArray.sort();
        window.survey = new Survey.Model(json);

        survey
            .onComplete
            .add(function (result) {
                // document
                //     .querySelector('#surveyResult')
                //     .innerHTML = "result: " + JSON.stringify(result.data);
                // console.log(result.data);
                var data = result.data;
                data["city"] = city;
                $.ajax({
                    url: "/survey_submit/",
                    method: "POST",
                    data: data,
                }).done(function (response) {

                    window.location.href = "/pastTravels/";
                });
            });

        survey.data = {
            cuisines: []
        };

        $("#surveyElement").Survey({model: survey});

        console.log(cuisinesArray);
    });
});


var json = {
    "elements": [
        {
            "type": "tagbox",
            "isRequired": true,
            "name": "cuisines",
            "choices": cuisinesArray,
            "title": "Which cuisines would you prefer?"
        },
        {
            "type": "barrating",
            "isRequired": true,
            "name": "rating",
            "ratingTheme": "fontawesome-stars",
            "title": "How important are user reviews to you?",
            "choices": ["1", "2", "3", "4"]
        },
        {
            "type": "boolean",
            "name": "bool",
            "title": "Please answer the question",
            "label": "I would like to explore different cuisines.",
            "isRequired": true
        }
    ]
};
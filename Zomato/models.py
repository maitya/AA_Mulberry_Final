"""
Contains the models for the pastTravels.

Some code was taken from this tutorial:
https://docs.djangoproject.com/en/2.0/ref/contrib/gis/tutorial/
"""
from django.contrib.gis.db import models
from PastTravels.models import WorldBorder
from django.contrib.gis.geos import Point

class City(models.Model):
    """TODO."""
    country = models.ForeignKey(WorldBorder, on_delete=models.CASCADE)
    name = models.CharField('City Name', max_length=125)
    zomato_id = models.IntegerField('Zomato city id')

    def __str__(self):
        return self.name

    def getId(self):
        return self.zomato_id


class Cuisine(models.Model):
    """TODO."""
    name = models.CharField('Cuisine Name', max_length=125)
    zomato_id = models.IntegerField('Zomato cuisine id')

    def __str__(self):
        return self.name

    def getId(self):
        return self.zomato_id


class CityCuisine(models.Model):
   """TODO."""
   city = models.ForeignKey(City, on_delete=models.CASCADE)
   cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE)

   def __str__(self):
       return "{}: {}".format(
           self.city.name,
           self.cuisine.name
       )

   def getId(self):
       return self.zomato_id


class ZomatoRestaurant(models.Model):
    """TODO."""
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField('Restaurant Name', max_length=125)
    address = models.CharField('Restaurant address',max_length=400, default="not available")
    zomato_id = models.IntegerField('Zomato Restaurant id')
    position = models.PointField('location', null=True)
    cuisines = models.CharField('Cuisine Names', max_length=400, default="not available")
    thumb = models.CharField('thumbnail',max_length=400,default="")
    agg_rating = models.FloatField('Zomato review rating',default=0.0)
    votes = models.IntegerField('number of votes',default=0)
    cost_for_two = models.FloatField('Zomato cost for two',default=0.0)
    currency = models.CharField('currency', max_length=50, default="N.A")



    def __str__(self):
        return self.name

    def getId(self):
        return self.zomato_id


class CountryToCuisineMap(models.Model):
    """Map from country to cuisine."""
    country = models.ForeignKey(WorldBorder, on_delete=models.CASCADE)
    cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE)

    def __str__(self):
        return self.country.name
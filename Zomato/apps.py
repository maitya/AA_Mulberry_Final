import csv
import os

from django.apps import AppConfig

from PastTravels.models import WorldBorder
from Zomato.models import Cuisine, CountryToCuisineMap


class ZomatoConfig(AppConfig):
    name = 'Zomato'


def load_country_2_cuisine():
    if CountryToCuisineMap.objects.all().count() != 0:
        return
    country2_cuisine_path = os.path.abspath(os.path.join(
        os.path.dirname(__file__), 'data',
        'country2cuisine.csv'))
    with open(country2_cuisine_path, "r") as csvFile:
        reader = csv.DictReader(csvFile)
        for row in reader:
            country = row["Country"]
            cuisine = row["Nationality"]

            if (WorldBorder.objects.filter(name=country).exists() and
                Cuisine.objects.filter(name=cuisine).exists()):
                print(country)
                CountryToCuisineMap.objects.get_or_create(
                    country=WorldBorder.objects.get(name=country),
                    cuisine=Cuisine.objects.get(name=cuisine))
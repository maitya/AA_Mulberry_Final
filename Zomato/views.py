from django.shortcuts import render
from AdaptiveTravelPlanner import urls
from .models import Cuisine

# Create your views here.

cuisines = ["Cuinine {}".format(i) for i in range(10)]


def simplePageZomato(request):
    # user = auth.getcuurentuser()
    # cuisines = [c.name for c in Cuisine.objects.all().filter(user_id=user)]
    cuisines = [c.name for c in Cuisine.objects.all()]

    return render(request, "simplePageZomato.html", {"cuisines": cuisines})

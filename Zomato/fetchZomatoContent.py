#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 14:07:56 2018

@author: unlearn
"""
import json
import requests


class ZomatoContentFetcher:
    API_CITY = 'https://developers.zomato.com/api/v2.1/cities?q='
    API_CUISISNE = 'https://developers.zomato.com/api/v2.1/cuisines?city_id='
    API_SEARCH = 'https://developers.zomato.com/api/v2.1/search?'#entity_id=91&entity_type=city&cuisines=25%2C60'
    API_RESTAURANT = 'https://developers.zomato.com/api/v2.1/search?entity_id='
    Cities = {"Dublin": "Ireland", "London":"United Kingdom", "Sydney, NSW" : "Australia"}

    city_ids = dict()
    cuisine_ids = dict()
    userModel = dict()
    User_key = {"user-key": "083bbf7c39f575e51af5b62ac893f055"}


    def getCityIds(self):
        for city in self.Cities.keys():
            r = requests.get(self.API_CITY + city, headers=self.User_key)
            data = r.json()
            suggestions = data['location_suggestions']
            for i in suggestions:
                if i['country_name'] == self.Cities.get(city):
                    self.city_ids[i['id']] = city
        return self.city_ids


    def inflateuserModelCatagories(self):
        self.userModel = dict()
        for ids in self.city_ids.keys():
            r = requests.get(self.API_CUISISNE + str(ids), headers=self.User_key)
            data = r.json()
            for l in data["cuisines"]:
                item = l['cuisine']
                self.userModel[item['cuisine_name']] = '0'
                self.cuisine_ids[item['cuisine_name']] = item['cuisine_id']
        return self.userModel


    def initializeuserModel(self,cuisineList):
        for i in cuisineList:
            self.userModel[i] = str(int(self.userModel.get(i)) + 1)


    def suggestRestaurants(self,userModel, cityid):
        r = requests.get(self.API_SEARCH, headers=self.User_key)
        data = r.json()

    def getCuisines(self, id):
        self.getCityIds()
        usermod = self.inflateuserModelCatagories()
        #self.initializeuserModel({'Korean', 'Canadian', 'Chinese', 'Japanese'})
        self.suggestRestaurants(self.userModel, id)
        return usermod

    def getIds(self):
        self.getCityIds()
        inverted = {self.city_ids[k] : k for k in self.city_ids}
        return inverted

    #load 20 restaurants per cuisine
    def getRestaurantByCity(self, id, cuisineIDs):
        data = []
        for cuisine_id in cuisineIDs:
            r = requests.get(self.API_SEARCH + "&entity_id="+ str(id) +"&entity_type=city" + "&cuisines=" + str(cuisine_id), headers=self.User_key)
            x = r.json()
            data = data + x["restaurants"]
        print(len(data))
        return data
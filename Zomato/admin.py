from django.contrib.gis import admin
from .models import City, CityCuisine, Cuisine, ZomatoRestaurant,\
    CountryToCuisineMap

# Register your models here.
admin.site.register([City, CityCuisine, Cuisine, ZomatoRestaurant,
                     CountryToCuisineMap], admin.GeoModelAdmin)
from django.core.management.base import BaseCommand
from Zomato.apps import load_country_2_cuisine


class Command(BaseCommand):
    def handle(self, *args, **options):
        load_country_2_cuisine()


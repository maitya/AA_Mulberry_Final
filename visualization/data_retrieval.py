import json
import os
import requests

def getDataFromWeb():
    # read url
    with open(os.path.join("visualization", "static", "data", "url_request.txt"), "r") as urlFile:
        url = urlFile.read()
    # fetch contents
    response = requests.get(url)
    # store results
    with open(os.path.join("visualization", "static", "data", "output.txt"), "w") as outputFile:
        outputFile.write(json.dumps(response.json(), indent=4))
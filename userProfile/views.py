
from django.contrib.auth.forms import UserCreationForm
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.models import User
from django.urls import reverse
from django.views.generic import CreateView, DetailView, View
from survey.views import showSurvey
from django.db.models import Count
import datetime

from UserModel.models import FirstLogin


class RegisterView(SuccessMessageMixin, CreateView):
    form_class = UserCreationForm
    template_name = 'registration/register.html'
    success_url = '/login'
    success_message = "Your account was created successfully."

    def dispatch(self, *args, **kwargs):
        return super(RegisterView, self).dispatch(*args, **kwargs)


class ProfileDetailView(DetailView):
    template_name = 'profile/user.html'

    def get_object(self):
        user = self.request.user
        return user



class HomeView(DetailView):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            firstlogin_obj, _ = FirstLogin.objects.get_or_create(
                user=request.user, defaults=dict(first_login=True))
            if firstlogin_obj.first_login:
                return showSurvey(request)

        # in all other cases
        return HttpResponseRedirect(reverse('recommendation_search'))

function draw(user_model) {

    console.log(user_model);

    var options = {

         responsive: true,
    maintainAspectRatio: true,
        scale: {
            ticks: {
                beginAtZero: true,
                max: 1,
                fontSize: 15

            },
            pointLabels: { fontSize:10 }
        }

    };

    var data = {
        labels: user_model["cuisines"],
        datasets: [{
            data: user_model["weights"],
            borderColor:"rgba(75,192,192,1)",
            pointBorderWidth:1,
            pointHoverRadius: 1,
            pointHoverBackgroundColor: "#edffe6",
            borderCapStyle: 'butt',
             backgroundColor: [
                'rgba(237, 255, 230, 0.2)',

            ],

             borderColor: [
                'rgba(255,99,132,1)',

            ],
        }]
    };

    var ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
}
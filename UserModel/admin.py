from django.contrib import admin
from .models import FirstLogin, UserCuisine, UserDetail, Userlikes, Userdislikes

admin.site.register(FirstLogin)
admin.site.register(UserDetail)
admin.site.register(UserCuisine)
admin.site.register(Userlikes)
admin.site.register(Userdislikes)
import pandas as pd
from Zomato.models import Cuisine
import os
from collections import OrderedDict

class Cuisines:

    cuisineList = ['Irish', 'Japanese', 'British', 'Italian', 'Thai', 'French', 'Spanish', 'Indonesian', 'Chinese', 'Mexican', 'Indian', 'American', 'Middle Eastern', 'Vietnamese', 'Moroccan', 'Australian', 'Brazilian', 'Polish', 'Pakistani', 'Turkish']
    fileName = os.path.abspath(os.path.join(
             os.path.dirname(__file__), 'data',
             'distanceMatrix.csv'))

    def getFinalCuisines(self):
        return self.cuisineList

    def getDistanceMatrix(self):
        dm = pd.read_csv(self.fileName, sep=',')
        dm = dm.values
        return dm

    #input 2 cuisine objects, returns distance between them
    def getDistanceBetweenCuisines(self, cuisine1, cuisine2):
        name1 = cuisine1.name
        name2 = cuisine2.name
        matrix = self.getDistanceMatrix()
        cuisineList = self.cuisineList
        index1 = cuisineList.index(name1)
        index2 = cuisineList.index(name2)
        return matrix[index1, index2]

    #input UserModel weights, returns the 5 closest cuisines overall
    def getClosestCuisinesByWeight(self, cuisines):
        similarList = {} #dict with (key, value) as (cuisine, distance)
        closestList = []

        remaining = set(self.cuisineList) - set(k.name for k, v in cuisines)

        for cuisine, weight in cuisines:
            for item in remaining:
                cuisine2 = Cuisine.objects.get(name=item)
                distance = self.getDistanceBetweenCuisines(cuisine, cuisine2)
                if(distance > 0):
                    if cuisine2 not in similarList:
                        similarList[cuisine2] = distance/weight
                    else:
                        similarList[cuisine2] = min(similarList[cuisine2], distance/weight)

        #sort dict by distance, return the 5 closest cuisines
        for item in sorted(similarList, key=similarList.get):
            closestList.append((item, similarList[item]))
        return closestList[:5]

    def getClosestCuisines(self, cuisine):
        similarList = {} #dict with (key, value) as (cuisine, distance)
        closestList = []
        finalCuisineList = self.cuisineList

        for item in finalCuisineList:
            cuisine2 = Cuisine.objects.get(name=item)
            distance = self.getDistanceBetweenCuisines(cuisine, cuisine2)
            if(distance > 0):
                similarList[cuisine2] = distance

        #sort dict by distance, return the 5 closest cuisines
        for item in sorted(similarList, key=similarList.get):
            closestList.append((item, similarList[item]))
        return closestList[:2]
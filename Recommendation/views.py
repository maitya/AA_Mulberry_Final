import json
from math import ceil
import numpy
import random

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.http import JsonResponse, Http404
from django.shortcuts import render
from UserModel.apps import create_cuisine_instance, get_user_model, \
    delete_cuisine_instance, add_restaurant_to_likes, \
    remove_restaurant_from_likes, remove_restaurant_from_dislikes, \
    add_restaurant_to_dislikes
from UserModel.models import UserCuisine
from Zomato.models import Cuisine, City, ZomatoRestaurant
from Recommendation.cuisineLoader import Cuisines

# Create your views here.
@login_required
def search_view(request):
    return render(request, 'recommendation.html', {})

def dict_of_restaurant(resto, cuisine, origin):
    return dict(
        name=resto.name,
        address=resto.address,
        cuisines=cuisine.name,
        cuisine_id=cuisine.id,
        resto_id=resto.id,
        thumb=resto.thumb,
        rating=resto.agg_rating,
        nb_votes=resto.votes,
        cost_for_two=str(resto.cost_for_two) + resto.currency,
        origin=origin,
    )


def basic_search(user, city):
    total_results_desired = 12

    cuisineHandler = Cuisines()
    # get all restaurants for one city
    restaurants = list(ZomatoRestaurant.objects.filter(city=city))

    # get cuisine
    resto_cuisine = {}
    for resto in restaurants:
        cuisines_str = [r.strip() for r in resto.cuisines.split(',')]
        for c in cuisines_str:
            try:
                c_obj = Cuisine.objects.get(name=c)
            except ObjectDoesNotExist:
                continue
            list_ = resto_cuisine.get(c_obj, set())
            list_.add(resto)
            resto_cuisine[c_obj] = set(list_)

    model = get_user_model(user.id)

    ## FILTER RESULTS

    results_prob = .66

    number_results = ceil(total_results_desired * round(results_prob, 2))
    number_local = 0
    number_random = 0

    if(number_results < total_results_desired):
        remainder = total_results_desired - number_results
        if model['trylocal']:
            local_prob = .5
            number_local = ceil(remainder*local_prob)
            random_prob = .5
        else:
            random_prob = 1
        number_random = ceil(remainder * random_prob)


    print("Results: ", number_results)
    print("Local: ", number_local)
    print("Random: ", number_random)

    print("weights", model['weights'])
    weights = model['weights']
    total_weights = sum(weights.values())

    #generating the "taste" results
    cuisineProbDist = []
    for k, w in weights.items():
        cuisineProbDist.append(float(w / total_weights))

    user_cuisines = [k for k, w in weights.items()]

    chosen_cuisines = numpy.random.choice(user_cuisines, number_results,
                                          p=cuisineProbDist)
    print("chosen cuisine")
    selected_restos = set()
    for cuisine in chosen_cuisines:
        if len(resto_cuisine[cuisine]) == 0:
            continue
        resto = random.choice(list(resto_cuisine[cuisine]))
        resto_cuisine[cuisine].remove(resto)
        selected_restos.add((resto, cuisine))


    #generating the "discover" results
    other_cuisines = cuisineHandler.getClosestCuisinesByWeight(weights.items())
    print("5 most similar cuisines: ", other_cuisines)

    total_similar = [w for k,w in other_cuisines]

    similarProbDist = []
    for k, w in other_cuisines:
        similarProbDist.append(float(w/sum(total_similar)))

    similar_cuisines = [k for k,w in other_cuisines]
    if len(similar_cuisines) > 0:
        chosen_similar = numpy.random.choice(similar_cuisines, number_local,
                                             p=similarProbDist)
    else:
        chosen_similar = []

    other_restaurants = set()
    for cuisine in list(chosen_similar):
        if len(resto_cuisine[cuisine]) == 0:
            continue
        resto = random.choice(list(resto_cuisine[cuisine]))
        resto_cuisine[cuisine].remove(resto)
        other_restaurants.add((resto, cuisine))


    #generating the "random" results

    random_cuisines = (set(Cuisine.objects.all())
                       - set(k for k in user_cuisines)
                       - set(other_cuisines))
    print("after random cuisines")

    random_restaurants = set()
    for cuisine in random.sample(random_cuisines, number_random):
        if len(resto_cuisine[cuisine]) == 0:
            continue
        resto = random.choice(list(resto_cuisine[cuisine]))
        resto_cuisine[cuisine].remove(resto)
        random_restaurants.add((resto, cuisine))

    #populating the final_results
    final_results = []
    for r, c in sorted(selected_restos, key=lambda x: x[1].name):
        final_results.append(
            dict_of_restaurant(r, c, 'taste'))

    for r, c in sorted(other_restaurants, key=lambda x: x[1].name):
        final_results.append(
            dict_of_restaurant(r, c, 'discover'))

    for r, c in sorted(random_restaurants, key=lambda x: x[1].name):
        final_results.append(
            dict_of_restaurant(r,c,'random')
        )

    return final_results


@login_required
def get_results(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        city = City.objects.get(name=data['city'])

        restaurant_list = basic_search(request.user, city)
        # save in session the restaurant list
        request.session['search_results'] = restaurant_list
        request.session['feedbacks'] = json.dumps({})

        return JsonResponse({'results': restaurant_list})
    return Http404("Page requires POST info")


positive_feedback = 1.0
negative_feedback = 0.0
@login_required
def resto_feedback(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        restaurant_list = request.session['search_results']
        resto_id = data['resto-id']
        resto = restaurant_list[int(resto_id)]
        feedback_type = data['feedback']

        feedbacks = json.loads(request.session['feedbacks'])
        print("feedbacks: ", feedbacks)
        previous_feedback_id = feedbacks.get(resto_id, None)
        print("previous feedback id", previous_feedback_id)
        if previous_feedback_id is not None:
            delete_cuisine_instance(request.user, int(previous_feedback_id))


        #also change weights of similar cuisines based on the feedback
        cuisine = Cuisine.objects.get(id=resto['cuisine_id'])
        neighbours = Cuisines().getClosestCuisines(cuisine)

        remove_restaurant_from_likes(request.user, resto["resto_id"])
        remove_restaurant_from_dislikes(request.user, resto["resto_id"])

        if feedback_type == 1:
            exp_id = create_cuisine_instance(request.user, cuisine, positive_feedback)
            feedbacks[resto_id] = exp_id

            add_restaurant_to_likes(request.user, resto["resto_id"])
            for item, weight in neighbours:
                create_cuisine_instance(request.user, item, min(1, positive_feedback/weight))

        elif feedback_type == -1:
            exp_id = create_cuisine_instance(request.user, cuisine, negative_feedback)
            feedbacks[resto_id] = exp_id

            add_restaurant_to_dislikes(request.user, resto["resto_id"])

            # for item, weight in neighbours:
            #     create_cuisine_instance(request.user, item, max(negative_feedback, 0.1/weight))

        elif feedback_type == 0:
            # don't do anything
            pass

        print(cuisine.name, feedback_type)
        request.session['feedbacks'] = json.dumps(feedbacks)
        return JsonResponse({'response': True})
    return Http404("Page requires POST info")


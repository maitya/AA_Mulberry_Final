from django.urls import path
from .views import search_view, get_results, resto_feedback


urlpatterns = [
    path('', search_view, name="recommendation_search"),
    path('search', get_results, name="get_results"),
    path('resto_feedback', resto_feedback, name="resto_feedback"),
]
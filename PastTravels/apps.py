import json
import time
import math
from datetime import datetime

from django.apps import AppConfig

from PastTravels.models import WorldBorder, VisitedCountry
from django.contrib.gis.geos import Point, MultiPoint

from UserModel.apps import create_cuisine_instance
from Zomato.models import Cuisine, CountryToCuisineMap
from Zomato.apps import load_country_2_cuisine


class PasttravelsConfig(AppConfig):
    name = 'PastTravels'


def get_country(wkt):
    countries = list(WorldBorder.objects.filter(mpoly__contains=wkt))
    return countries[0] if len(countries) > 0 else None


class Locations(object):
    def __init__(self, geojsonPath):
        takeout = open(geojsonPath, 'r')
        self.locations = json.load(takeout)['locations']

    def filter_on_accuracy(self, accuracy=15):
        self.locations = [loc for loc in self.locations
                         if loc['accuracy'] < accuracy]
        self.locations.sort(key=lambda x:int(x['timestampMs']))

    def get_one_point_per_day(self):
        """"Retrieve one element per 24 hour."""
        if len(self.locations) == 0:
            return
        filtered = []
        delta = 3600 * 1000 * 24
        filtered.append(self.locations[0])
        nextday = int(self.locations[0]['timestampMs']) + delta
        for loc in self.locations:
            ts = int(loc['timestampMs'])
            if ts < nextday:
                continue
            filtered.append(loc)
            nextday = ts + delta
        return filtered


def get_countries_from_points(points):
    countries = {}
    for loc in points:
        point = Point(
            float(loc['longitudeE7']) * 1e-7,
            float(loc['latitudeE7']) * 1e-7,
            srid=4326  # WGS 84
        )
        country = get_country(point)
        if country is None:
            continue

        list_ = countries.get(country, [])
        list_.append(loc)
        countries[country] = list_
    return countries


def update_user_visited_countries(countries, trips, user):
    total = sum([len(points) for points in countries.values()])
    for country, points in countries.items():
        number = len(points)
        VisitedCountry.objects.update_or_create(
            country=country,
            user=user,
            defaults=dict(
                number=number,
                ratio=float(number) / total,
                date_start=datetime.fromtimestamp(
                    float(points[0]['timestampMs']) / 1000.
                ).date(),
                type="trip" if country in trips else "lived",
                enabled=country in trips

            )
        )
    print("database VisitiedCountry updated!")


def get_continuous_trips(countries, country_name, verbose=True):
    current_country = [country for country in countries
                  if country.name == country_name]
    if len(current_country) == 0:
        return None
    current_country = current_country[0]

    points = countries[current_country]

    # compute trips, a trip is a continuous presence in a country every day.
    trips = []
    begin = int(points[0]['timestampMs'])
    end = begin
    for pt in points:
        ptTs = int(pt['timestampMs'])
        # find the end of the trip, begin one day not logged (48h)
        if ptTs - end > 1000 * 3600 * 72:
            trips.append((begin, end + 1000 * 3600 * 24))
            begin = ptTs
            end = ptTs
        else:
            end = ptTs

    trips.append((begin, end + 1000 * 3600 * 24))
    if verbose:
        for firstPoint, lastPoint in trips:
            print("Found trip between {} and {}".format(
                datetime.fromtimestamp(
                    float(firstPoint) / 1000.),
                datetime.fromtimestamp(
                    float(lastPoint) / 1000.),
            ))

    return trips, current_country


def refine_trips_times(locations, trips, country):
    """refine the beginning/end of a trip."""
    margin = 1000 * 3600 * 25  # 25 hours

    trip_points = []

    for pt in locations.locations:
        time_stamp = int(pt['timestampMs'])
        in_trip = False
        for firstPoint, lastPoint in trips:
            if (abs(firstPoint - time_stamp) < margin
                    or abs(lastPoint - time_stamp) < margin):
                in_trip = True
                break
        if not in_trip:
            continue

        # test uk
        point = Point(
            float(pt['longitudeE7']) * 1e-7,
            float(pt['latitudeE7']) * 1e-7,
            srid=4326  # WGS 84
        )

        if get_country(point) != country:
            continue

        trip_points.append(pt)

    print("Found {} points in UK".format(len(trip_points)))
    return trip_points


def update_country_points_in_db(points, country, user):

    print("Inserting all uk points in the DB.")
    mpt = MultiPoint([])
    for point in points:
        point = Point(
            float(point['longitudeE7']) * 1e-7,
            float(point['latitudeE7']) * 1e-7,
            srid=4326  # WGS 84
        )
        mpt.append(point)

    VisitedCountry.objects.update_or_create(
        country=country,
        user=user,
        defaults=dict(
            geo_points=mpt
        )
    )


def get_trip_lived_countries(countries, verbose=True):
    """For all countries, determine if the visit a trip or for living."""
    trips_days = {}
    lived_days = {}
    # threshold, a trip < 60 day, else lived in the country
    threshold_days = 60
    for country in countries:
        print("processing country", country)
        trips, _ = get_continuous_trips(countries, country.name, verbose=False)
        for start, end in trips:
            duration_days = math.ceil((end - start) / (1000 * 3600 * 24))
            # print("duration=", duration_days)
            date = datetime.fromtimestamp(float(start) / 1000.).date()
            year = date.year

            if duration_days < threshold_days:
                list_ = trips_days.get(country, [])
                list_.append((year, duration_days))
                trips_days[country] = list_
            else:
                list_ = lived_days.get(country, [])
                list_.append((year, duration_days))
                lived_days[country] = list_
    if verbose:
        print("trip:", "\n".join([str(t) for t in trips_days.items()]))
        print("lived:", "\n".join([str(l) for l in lived_days.items()]))
    return trips_days, lived_days


def get_visited_countries(user_id):
    """Get the names of visited countries."""
    visited_countries = VisitedCountry.objects.filter(
        user_id=user_id, enabled=True)
    return [vc.country for vc in visited_countries]


def load_travels_from_file(takeout_json, user=None):

    VisitedCountry.objects.all().filter(user=user).delete()
    start = time.time()

    locations = Locations(takeout_json)
    locations.filter_on_accuracy(accuracy=15)

    end = time.time()
    print("loaded {} locations in {}".format(len(locations.locations),
                                             end - start))

    # 14.92M locations in 32s
    # 447k in 39s
    # full: 628k locations
    # default json, 447k in 3s

    start = time.time()
    one_per_day = locations.get_one_point_per_day()
    end = time.time()
    print("loaded {} days in {}".format(len(one_per_day),
                                        end - start))

    # 1 per day: 1000 locations
    # every 2h: 10.7k locations

    start = time.time()
    countries = get_countries_from_points(one_per_day)
    end = time.time()
    print("found {} countries in {}".format(len(countries),
                                            end - start))
    # print([(k.name, v) for k, v in countries.items()])


    countries_trip, countries_lived = get_trip_lived_countries(countries,
                                                               False)
    c_lived = set(countries_lived.keys())
    print("c_lived", c_lived)
    print("trip before filter", countries_trip.keys())
    countries_trip = [c for c in countries_trip.keys() if c not in c_lived]

    print("trip after filter:", countries_trip)
    print("lived:", countries_lived.keys())
    update_user_visited_countries(countries, countries_trip, user)

    countries_names = [c.name for c in countries]

    if "United Kingdom" in countries_names:
        uk_trips, uk = get_continuous_trips(countries, "United Kingdom")
        all_trips = refine_trips_times(locations, uk_trips, uk)
        update_country_points_in_db(all_trips, uk, user)


def map_country_to_cuisine(countries):
    """Map each country to cuisine and return list of cuisines."""
    cuisines = []
    for country in countries:
        print("loop: country=", country)
        if (CountryToCuisineMap.objects.filter(country=country).exists()):
            obj = CountryToCuisineMap.objects.get(country=country)
            cuisines.append(obj.cuisine)
    return cuisines


def update_user_model_from_travels(user):
    """Update the usermodel using the data from the past travels."""
    load_country_2_cuisine()
    visited_countries = get_visited_countries(user.id)
    cuisines = map_country_to_cuisine(visited_countries)
    for cuisine in cuisines:
        create_cuisine_instance(user, cuisine, weight=1)

"""
Contains the models for the pastTravels.

Some code was taken from this tutorial:
https://docs.djangoproject.com/en/2.0/ref/contrib/gis/tutorial/
"""
from django.contrib.gis import serializers
from django.contrib.gis.db import models
from django.conf import settings


class WorldBorder(models.Model):
    """
    Regular Django fields corresponding to the attributes in the
    world borders shapefile.
    """
    name = models.CharField(max_length=50)
    area = models.IntegerField()
    pop2005 = models.IntegerField('Population 2005')
    fips = models.CharField('FIPS Code', max_length=2)
    iso2 = models.CharField('2 Digit ISO', max_length=2)
    iso3 = models.CharField('3 Digit ISO', max_length=3)
    un = models.IntegerField('United Nations Code')
    region = models.IntegerField('Region Code')
    subregion = models.IntegerField('Sub-Region Code')
    lon = models.FloatField()
    lat = models.FloatField()

    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.MultiPolygonField()

    def __str__(self):
        """Returns the string representation of the model."""
        return self.name


class VisitedCountry(models.Model):
    """List a country visited by a user"""
    country = models.ForeignKey(WorldBorder, on_delete=models.CASCADE,
                               related_name='visited')
    number = models.IntegerField('Number of Visit')
    ratio = models.FloatField('Country importance')
    geo_points = models.MultiPointField('Travel path', null=True)
    date_start = models.DateField(null=True)
    type = models.CharField(max_length=60, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            on_delete=models.DO_NOTHING, null=True)
    enabled = models.BooleanField('enabled', default=True)

    def __str__(self):
       return "{}-{}: {}, {} ({:.0f}%) {}".format(
           self.user.username,
           self.date_start,
           self.country,
           self.number,
           100. * self.ratio,
           self.type)
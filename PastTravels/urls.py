from django.urls import path
from .views import worldGeoJson, upload_takeout, pastTravels
from .views import update_enabled_country


urlpatterns = [
    path('', pastTravels, name="past_travels"),
    path('worldjson', worldGeoJson, name='worldjson'),
    path('loadtakeout', upload_takeout, name='loadtakeout'),
    path('update_enabled_country', update_enabled_country,
         name='update_enabled_country')
]
from django.core.management.base import BaseCommand

from PastTravels.worldGeom import run


class Command(BaseCommand):
    def handle(self, *args, **options):
        run()




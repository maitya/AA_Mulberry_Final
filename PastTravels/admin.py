from django.contrib.gis import admin
from .models import WorldBorder, VisitedCountry

admin.site.register(WorldBorder, admin.GeoModelAdmin)
admin.site.register(VisitedCountry, admin.GeoModelAdmin)


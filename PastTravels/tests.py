# Create your tests here.

from django.test import TestCase

import PastTravels.apps
import PastTravels.worldGeom
from PastTravels.apps import get_country
from django.contrib.gis.geos import Point


class WorldCountriesTest(TestCase):

    def setUp(self):
        """Fill db with test data."""
        PastTravels.worldGeom.run(verbose=False)

    def testUSA(self):
        """Test a point in USA."""
        country = "United States"
        point_wkt = 'POINT(-95.3385 29.7245)'

        cnty = get_country(point_wkt).name
        self.assertEqual(country, cnty)

    def testFrance(self):
        """Test a point in France."""
        country = "France"
        pnt = Point(12345., 5346400., srid=32632)

        cnty = get_country(pnt).name
        self.assertEqual(country, cnty)

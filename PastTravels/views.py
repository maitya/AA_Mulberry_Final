from django.http import JsonResponse, HttpResponseRedirect
from django import forms
from django.shortcuts import render
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from PastTravels.apps import update_user_model_from_travels
from .apps import load_travels_from_file
import json

# Create your views here.
from PastTravels.models import VisitedCountry


def pagesIndex(request):
    from AdaptiveTravelPlanner import urls
    pages = []
    for d in urls.urlpatterns:
        txt = d.pattern.regex.pattern.replace("^", "").replace(
            "$", "").replace("\/", "/")
        txt = "/" + txt if txt else "/"
        pages.append(txt)
    return render(request, "pagesindex.html", {"pagesList": pages})


@login_required
def worldGeoJson(request):
    return JsonResponse(get_geojson_of_visited_countries(request.user.id), safe=False)


def get_geojson_of_visited_countries(user_id=4):
    visited_countries =  list(
        VisitedCountry.objects.all().filter(user_id=user_id))
    geojson = {"type": "FeatureCollection",
               "crs": {"type": "name", "properties": {"name": "EPSG:4326"}},
               "features": []}
    for c in visited_countries:
        feat = {'properties': {
            'name': c.country.name,
            'ctype': c.type
        },
            'type': "Feature",
            'geometry': json.loads(c.country.mpoly.geojson)
        }
        geojson["features"].append(feat)
        # print(c.country.name)
        # print(type(c.country.mpoly))

    # {"type": "FeatureCollection",
    #  "crs": {"type": "name", "properties": {"name": "EPSG:4326"}},
    #  "features": [
    #      {"properties": {}, "type": "Feature",
    #       "geometry": {"type": "MultiPolygon", "coordinates":[[ [[], []] ]]}}
    # ]}
    return json.dumps(geojson)


@login_required
def update_enabled_country(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        VisitedCountry.objects.all().filter(
            country__name=data['country'],
            user=request.user).update(enabled=data['checked'])
        checked = VisitedCountry.objects.get(country__name=data['country'],
                                   user=request.user).enabled
        return JsonResponse({'checked': checked})


@login_required
def pastTravels(request):
    visited_countries =  VisitedCountry.objects.all().filter(user=request.user)
    lived = [(c.country.name, "checked" if c.enabled else "") for c in visited_countries if c.type == "lived"]
    visited = [(c.country.name, "checked" if c.enabled else "") for c in visited_countries if c.type != "lived"]
    return render(request, "past_travels.html", {
        'lived': sorted(lived),
        'visited': sorted(visited)
    })


class UploadFileForm(forms.Form):
    file = forms.FileField()

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)


def handle_uploaded_file(f, user):
    tmpfile = 'tmp_takeout.json'
    with open(tmpfile, 'wb') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    load_travels_from_file(tmpfile, user)
    print("updating USER model")
    update_user_model_from_travels(user)


def upload_takeout(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES, user=request.user)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'], form.user)
            return HttpResponseRedirect(reverse('past_travels'))
    else:
        form = UploadFileForm()
    return render(request, 'travel_upload.html', {'form': form})
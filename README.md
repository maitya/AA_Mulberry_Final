Adaptive Application - Mulberry
==


Installing docker and docker-compose
--

* Install docker from here: 
[ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/#prerequisites),
[mac](https://docs.docker.com/docker-for-mac/install/)

For Ubuntu, once the package is install, follow the second [step](https://docs.docker.com/install/linux/linux-postinstall/).

* Install docker-compose>=1.19.0 with
```
sudo pip3 install docker-compose
```

Docker configuration
---
The configuration was created from [this tutorial](https://docs.docker.com/compose/django/).
the DB was upgraded to Postgis with [this image](https://github.com/appropriate/docker-postgis). 
The current configuration runs two images: `web` with Django and a Postgis server.
The `web` images has Python3 and all the dependencies present in the
 `requirements.txt` file.
 
The `web` image can be updated with
```
docker-compose build 
```

PyCharm integration
--
Once the project is loaded into PyCharm Pro, you can setup the 
Docker compose Python interpreter: go to settings, interpreter, add remote.
See [here](https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html#docker-compose-remote)
for detail.
Then, you can run any `manage.py` commands with CTRL+ALT+R
Try `makemigrations`, `migrate` or `createAdmin`


Updating the Django database
--
Run, wait for PostgreSQL to finish and you might need to run the commands again.
```
docker-compose run web ./manage.py makemigrations
docker-compose run web ./manage.py migrate
```

Running the Django server
--

Type:
```
docker-compose up
```

Run an interactive shell in the container
--
After running `docker-compose up`, run in a new terminal:
```
docker-compose exec web bash
```

Creating you own app
----
To create your own app, run the following command after
running `docker-compose up`:
```
docker-compose exec web python manage.py startapp <app name>
```
On Ubuntu or mac, after running the previous command, the new project will belong to root.
To fix this, run this:
```
sudo chown -R $USER:$USER .
```
Now add you new app to the django project settings
(`AdaptiveTravelPlanner/settings.py`) for example, for `pastTravels`:

```
For mac users to create a own app,once inside the root:
```
python manage.py startapp <appname>


INSTALLED_APPS = [
    ...
    'django.contrib.gis',
    'visualization',
    'pastTravels'
]
```

Creating an  admin superuser 
-
In PyCharm, Tools -> Run manage.py Tasks, type ```createAdmin```

